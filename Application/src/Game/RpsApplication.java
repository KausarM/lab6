package Game;

import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;

/*@author Kausar Mussa*/
public class RpsApplication extends Application {	
	public void start(Stage stage) {
		
		RpsGame alpha = new RpsGame (0,0,0);
		
		Group root = new Group(); 

		//scene is associated with container, dimensions
		Scene scene = new Scene(root, 650, 300); 
		scene.setFill(Color.BLACK);

		//associate scene to stage and show
		stage.setTitle("Rock Paper Scissors"); 
		stage.setScene(scene); 
		
		//create the buttons
		Button rockBut = new Button ("Rock");
		Button paperBut = new Button ("Paper");
		Button scissorsBut = new Button ("Scissors");
		
		//add buttons to the horizontal box
		HBox buttons = new HBox (rockBut,paperBut,scissorsBut);
		
		//creating the textField
		TextField message = new TextField ("Welcome!");
		message.setPrefWidth(200);
		TextField wins = new TextField ("Wins: ");
		wins.setPrefWidth(200);
		TextField losses = new TextField ("Losses: ");
		losses.setPrefWidth(200);
		TextField ties = new TextField ("Ties: ");
		ties.setPrefWidth(200);
		
		//adding the textfields to a second horizontalbox
		HBox texts = new HBox (message,wins,losses,ties);
		
		//adding the 2 HBox to the vertical box
		VBox page = new VBox (buttons,texts);
		
		//adding the VBox to the root
		root.getChildren().add(page);
		
		//actions for the buttons
		RpsGame input = new RpsGame (0,0,0);
		rockBut.setOnAction(new RpsChoice(message,wins,losses,ties,"rock", input));
		paperBut.setOnAction(new RpsChoice(message,wins,losses,ties,"paper", input));
		scissorsBut.setOnAction(new RpsChoice(message,wins,losses,ties,"scissors", input));
		
		stage.show(); 
	}
	
    public static void main(String[] args) {
        Application.launch(args);
    }
}    

