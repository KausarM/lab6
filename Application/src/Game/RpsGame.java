package Game;

import java.util.Random;

/*@author Kausar Mussa*/

public class RpsGame {
	
	private int wins;
	private int ties;
	private int losses;
	Random ranGenerator = new Random();
	
	public RpsGame ( int wins, int ties, int losses) {
		this.wins = wins;
		this.ties = ties;
		this.losses = losses;	
	}
	
	public int getWins() {
		return this.wins;
		
	}
	public int getTies() {
		return this.ties;
		
	}
	public int getLosses() {
		return this.losses;
		
}
	
	public String playRound (String choice) {
		int rock = 0;
		int paper = 1;
		int scissors = 2;
		int random = ranGenerator.nextInt(3);	
		String result= "";
		
		if (choice.equals("rock")) {
			if( random == rock) {
				result = "Both made the same choice. Tie";
				this.ties++;
			}
			else if (random == paper) {
				result = "Computer plays paper and won";
				this.losses++;
			}
			else if (random == scissors) {
				result = "Computer plays scissors and loses";
				this.wins++;
			}
		}
		 if (choice.equals("paper")) {
			if( random == rock) {
				result = "Computer plays rock and loses";
				this.wins++;
			}
			if (random == paper) {
				result = "Both made the same choice. Tie.";
				this.ties++;
			}
			if (random == scissors) {
				result = "Computer plays scissors and wins";
				this.losses++;
			}
		}
		 if (choice.equals("scissors")) {
				if( random == rock) {
					result = "Computer plays rock and wins";
					this.losses++;
				}
				if (random == paper) {
					result = "Computer plays paper and loses";
					this.wins++;
				}
				if (random == scissors) {
					result = "Both made the same choice. Tie";
					this.ties++;
				}
			}
		
		return result;
		
	}
	
}

	
	
	


