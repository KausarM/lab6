package Game;

import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;

/*@author Kausar Mussa*/
public class RpsChoice implements EventHandler<ActionEvent> {
	private TextField message;
	private TextField wins; 
	private TextField losses;
	private TextField ties;
	private String playerChoice;
	private RpsGame input;

	public RpsChoice (TextField message, TextField wins, TextField losses, TextField ties, String playerChoice, RpsGame input) {
		//constructor
		this.message=message;
		this.wins=wins;
		this.losses=losses;
		this.ties=ties;
		this.playerChoice=playerChoice;
		this.input=input;		
	}
	@Override
	public void handle (ActionEvent e) {
		
		int converter;
		
		String mess = this.input.playRound(playerChoice);
		this.message.setText(mess);
		
		converter = this.input.getWins();
		this.wins.setText("Wins: "+ (Integer.toString(converter)));
		
		converter = this.input.getLosses();
		this.losses.setText("Losses: "+ (Integer.toString(converter)));
		
		converter = this.input.getTies();
		this.ties.setText("Ties: " + (Integer.toString(converter)));
		
	}

}
